--[[

                            Minimalist HUD
                        Created by: SovietVallhund
                            Version 1.1.1
                        
]]

print( "======================" )
print( "----Minimalist HUD----")
print( "----Version 1.1.1-----")
print( "======================" )

--[[-------------------------------------------------------------------------
                                VARIABLES
---------------------------------------------------------------------------]]
local hide  = {
    CHudBattery         = true,
    CHudHealth          = true,
    CHudAmmo            = true,
    CHudSecondaryAmmo   = true,
}
local color_white   = Color( 255, 255, 255, 255 )
local hudHP         = 0
local hudArmor      = 0
local hudMoney      = 0
local hudSalary     = 0 
local circleMat     = Material( "minimalist/circle26x26.png" )

--[[-------------------------------------------------------------------------
                                 FONTS
---------------------------------------------------------------------------]]
surface.CreateFont( "SOV_HUD_SMALL", {
    font        = "DermaDefaultBold",
    size        = 16,
    weight      = 1000,
})

surface.CreateFont( "SOV_HUD_QUAN", {
    font        = "DermaDefaultBold",
    size        = 14,
    weight      = 500,
})

surface.CreateFont( "SOV_HUD_MED", {
    font        = "Trebuchet18",
    size        = 20,
    weight      = 1000,
})

surface.CreateFont( "SOV_HUD_EXCL", {
    font        = "Trebuchet18",
    size        = 25,
    weight      = 500,
})

surface.CreateFont( "SOV_HUD_LARGE", {
    font        = "Arial",
    size        = 40,
    weight      = 500,
})

--[[-------------------------------------------------------------------------
                            HIDE VANILLA HUD
---------------------------------------------------------------------------]]
hook.Add( "HUDShouldDraw", "HideHUD", function( name )
    if ( hide[ name ] ) then return false end
end)

--[[-------------------------------------------------------------------------
                            HUD PROPERTIES
---------------------------------------------------------------------------]]
hook.Add( "HUDPaint", "SOV_HUD", function()
    hudHP       = math.Clamp( Lerp( 3 * FrameTime(), hudHP, LocalPlayer():Health() ), 0, LocalPlayer():GetMaxHealth() )
    hudArmor    = math.Clamp( Lerp( 3 * FrameTime(), hudArmor, LocalPlayer():Armor() ), 0, 100 )
    hudMoney    = Lerp( 3 * FrameTime(), hudMoney, LocalPlayer():getDarkRPVar( "money" ) )
    hudSalary   = Lerp( 3 * FrameTime(), hudSalary, LocalPlayer().DarkRPVars.salary ) 

    -----> HUD Outline
    draw.RoundedBox( 0, 5, ScrH() - 111, 5, 104, Color( 40, 40, 40, 255 ) ) //    left border
    draw.RoundedBox( 0, 275, ScrH() - 111, 5, 104, Color( 40, 40, 40, 255 ) ) //  right border
    draw.RoundedBox( 0, 5, ScrH() - 115, 275, 5, Color( 40, 40, 40, 255 ) ) //    top border
    draw.RoundedBox( 0, 5, ScrH() - 10, 275, 5, Color( 40, 40, 40, 255 ) ) //     bottom border

    ----->HUD Surface
    draw.RoundedBox( 0, 10, ScrH() - 110, 265, 100, Color( 20, 20, 20, 240 ) )

    -----> HUD Health
    draw.RoundedBox( 0, 108, ScrH() - 32, 160, 13, Color( 40, 40, 40, 255 ) )
    draw.RoundedBox( 0, 109, ScrH() - 31, 158 * ( hudHP / LocalPlayer():GetMaxHealth() ), 11, Color( 215, 40, 40, 255 ) )
    draw.SimpleText( math.Round( hudHP ), "SOV_HUD_QUAN", 190, ScrH() - 33, color_white, TEXT_ALIGN_CENTER )
     
    -----> /HUD Armor
    draw.RoundedBox( 0, 108, ScrH() - 55, 160, 13, Color( 40, 40, 40, 255 ) )
	draw.RoundedBox( 0, 109, ScrH() - 54, 158 * ( hudArmor / 100 ), 11, Color( 32, 126, 189, 255 ) )
	draw.SimpleText( math.Round( hudArmor ), "SOV_HUD_QUAN", 190, ScrH() - 56, color_white, TEXT_ALIGN_CENTER )

    -----> Wanted Tab
    if LocalPlayer():isWanted() then
        draw.RoundedBox( 0, 5, ScrH() - 145, 120, 30, Color( 20, 20, 20, 240 ) )
        draw.RoundedBox( 0, 5, ScrH() - 145, 5, 31, Color( 40, 40, 40, 255 ) ) //       left border
        draw.RoundedBox( 0, 125, ScrH() - 145, 5, 31, Color( 40, 40, 40, 255 ) ) //     right border
        draw.RoundedBox( 0, 5, ScrH() - 150, 125, 5, Color( 40, 40, 40, 255 ) ) //      top border
        draw.SimpleText( "WANTED", "SOV_HUD_MED", 80, ScrH() - 141, Color( 150, 20, 15, 255 ), TEXT_ALIGN_CENTER )
        surface.SetMaterial( circleMat )
        surface.SetDrawColor( 255, 255, 255, 255 )
        surface.DrawTexturedRect( 13, ScrH() - 143, 26, 26 )
        draw.SimpleText( "!", "SOV_HUD_EXCL", 26, ScrH() - 143, color_white, TEXT_ALIGN_CENTER )
    end

    ----> Gun License Tab
    if LocalPlayer():getDarkRPVar( "HasGunlicense" ) then
        draw.RoundedBox( 0, 125, ScrH() - 145, 120, 30, Color( 20, 20, 20, 240 ) )
        draw.RoundedBox( 0, 125, ScrH() - 145, 5, 31, Color( 40, 40, 40, 255 ) ) //     left border
        draw.RoundedBox( 0, 245, ScrH() - 145, 5, 31, Color( 40, 40, 40, 255 ) ) //     right border
        draw.RoundedBox( 0, 125, ScrH() - 150, 125, 5, Color( 40, 40, 40, 255 ) ) //    top border
        draw.SimpleText( "Gun License", "SOV_HUD_MED", 188, ScrH() - 141, Color( 20, 150, 20, 255 ), TEXT_ALIGN_CENTER )
    end

    -----> Money & Salary
    draw.SimpleText( "Money:", "SOV_HUD_SMALL", 132, ScrH() - 81, color_white, TEXT_ALIGN_CENTER )
    draw.SimpleText( DarkRP.formatMoney( math.Round( hudMoney ) )  .. " (" .. DarkRP.formatMoney( math.Round( hudSalary ) )..")", "SOV_HUD_SMALL", 160, ScrH() - 81, color_white, TEXT_ALIGN_LEFT )
    
    -----> Job Title
    draw.SimpleText( "Job:", "SOV_HUD_SMALL", 121, ScrH() - 104, color_white, TEXT_ALIGN_CENTER ) 
	draw.SimpleText( LocalPlayer():getDarkRPVar( "job" ), "SOV_HUD_SMALL", 138, ScrH() - 104, color_white, TEXT_ALIGN_LEFT )

    ----> Ammo Display
    local wep = LocalPlayer():GetActiveWeapon()
    if IsValid( wep ) and ( wep:GetPrimaryAmmoType() > 0 ) then
        draw.RoundedBox( 0, ScrW() - 136, ScrH() - 61, 126, 51, Color( 20, 20, 20, 240 ) ) // surface
        draw.RoundedBox( 0, ScrW() - 141, ScrH() - 65, 5, 60, Color( 40, 40, 40, 255 ) ) //   left border
        draw.RoundedBox( 0, ScrW() - 10, ScrH() - 65, 5, 60, Color( 40, 40, 40, 255 ) ) //   right border
        draw.RoundedBox( 0, ScrW() - 141, ScrH() - 65, 132, 5, Color( 40, 40, 40, 255 ) ) //  top border
        draw.RoundedBox( 0, ScrW() - 141, ScrH() - 10, 132, 5, Color( 40, 40, 40, 255 ) ) //  bottom border
        draw.SimpleText( wep:Clip1() .. " / " .. LocalPlayer():GetAmmoCount( wep:GetPrimaryAmmoType() ), "SOV_HUD_LARGE", ScrW() - 73, ScrH() - 35, wep:Clip1() > 0 and color_white or Color( 255, 20, 20, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
    end
end)    

local function OpenPMPanel()
    local PMPanel = vgui.Create( "DModelPanel" )
    PMPanel:SetModel( LocalPlayer():GetModel() )
    PMPanel:SetPos( 10, ScrH() - 108 ) 
    PMPanel:SetSize( 96, 98 )
    PMPanel:ParentToHUD()
    function PMPanel:LayoutEntity( ent )
        return false
    end
    function PMPanel:Think()
        self:SetModel( LocalPlayer():GetModel() )
    end
    PMPanel:SetLookAt( Vector( -15, 3, 66 ) )
    PMPanel:SetCamPos( Vector( 94, -20, 50 ) )
    PMPanel:SetFOV( 10 )
end

hook.Add( "InitPostEntity", "SOV_OpenPMPanel", OpenPMPanel ) 